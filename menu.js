// const CURRENT_SERVER = 'http://localhost:8081';
const CURRENT_SERVER = 'http://192.168.0.16:8081';
// const CURRENT_SERVER = 'https://practical-bolt-244414.appspot.com/';
const instance = axios.create({
    baseURL: CURRENT_SERVER,
    timeout: 3000,
    headers: {
        'Access-Control-Allow-Origin': '*'
    }
});
const socket = io(CURRENT_SERVER);

let isCheckedRole = true;
let currentPlayerId = null; // for window event

console.log('203');

var menuState = {
    preload: function() {
        game.canvas.style.cursor = "none";
        game.load.image('background', 'assets/menu/menu&characters.png');
        game.load.image('title', 'assets/menu/charTitle.png');
        game.load.image('cursor', 'assets/menu/cursor.png');
        game.load.image('singleUser', 'assets/menu/singlePlayer.png');
        game.load.image('multipleUser', 'assets/menu/multiplePlayer.png');
        game.load.spritesheet('explosion', 'assets/menu/explosion.png', 300, 300);
        game.load.spritesheet('chicken', 'assets/characters/chicken.png', 185, 320);
        game.load.spritesheet('horse', 'assets/characters/horse.png', 226, 320);
        game.load.spritesheet('sheep', 'assets/characters/sheep.png', 184, 320);
        game.load.spritesheet('raccoon', 'assets/characters/raccoon.png', 225, 300);
        game.load.spritesheet('chickenDance', 'assets/characters/chickenDance.png', 130, 180);
        game.load.spritesheet('horseDance', 'assets/characters/horseDance.png', 130, 180);
        game.load.spritesheet('sheepDance', 'assets/characters/sheepDance.png', 130, 180);
        game.load.spritesheet('raccoonDance', 'assets/characters/raccoonDance.png', 130, 180);


    },
    create: async function() {
        this.p = 1;
        this.enterDown = false;
        this.mode = 'single';
        game.global.playerId = 1;
        game.global.whatISelected = null;
        game.global.whatOtherSelected = [];
        
        this.bg = game.add.image(0, 0, 'background');
        
        this.startLabel = game.add.text(460, 320, 'START', { font: '40px Optima', fill: '#ffffff'});
        this.startLabel.anchor.setTo(0.5, 0.5);
        this.startLabel.fontWeight = 'bold';
        this.grd = this.startLabel.context.createLinearGradient(0, 0, 0, this.startLabel.height);
        this.grd.addColorStop(0, '#ffffff');   
        this.grd.addColorStop(1, '#a2a5a8');
        this.startLabel.fill = this.grd;
        this.startLabel.setShadow(0, 0, '#000000', 8);
        game.add.tween(this.startLabel.scale).to({x: 1.1, y: 1.1}, 600).yoyo(true).loop().start();

        this.enterButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        this.title = game.add.image(0, 0, 'title');
        this.title.anchor.setTo(0.5, 0);
        this.title.reset(game.width/2, 0);
        this.title.alpha = 0;


        this.chicken = game.add.sprite(-500, -500, 'chicken');
        this.chicken.scale.setTo(0.6);
        this.chicken.inputEnabled = true;
        this.chicken.alpha = 0;
        this.chickenDance = game.add.sprite(160, 205, 'chickenDance');
        this.chickenDance.alpha = 0;
        this.chickenDance.animations.add('dance');
        this.chickenDance.scale.setTo(1.4);
        this.horse = game.add.sprite(-500, -500, 'horse');
        this.horse.scale.setTo(0.6);
        this.horse.inputEnabled = true;
        this.horse.alpha = 0;
        this.horseDance = game.add.sprite(330, 250, 'horseDance');
        this.horseDance.alpha = 0;
        this.horseDance.animations.add('dance');
        this.horseDance.scale.setTo(1.2);
        this.sheep = game.add.sprite(-500, -500, 'sheep');
        this.sheep.scale.setTo(0.6);
        this.sheep.inputEnabled = true;
        this.sheep.alpha = 0;
        this.sheepDance = game.add.sprite(490, 265, 'sheepDance');
        this.sheepDance.alpha = 0;
        this.sheepDance.animations.add('dance');
        this.sheepDance.scale.setTo(1.1);
        this.raccoon = game.add.sprite(-500, -500, 'raccoon');
        this.raccoon.scale.setTo(0.6);
        this.raccoon.inputEnabled = true;
        this.raccoon.alpha = 0;
        this.raccoonDance = game.add.sprite(650, 280, 'raccoonDance');
        this.raccoonDance.alpha = 0;
        this.raccoonDance.animations.add('dance');
        this.raccoonDance.scale.setTo(1);
        this.explosion = game.add.group();
        this.explosion.createMultiple(5, 'explosion');
        this.explosion.forEach(this.explosionSet, this);

        this.chicken.events.onInputOver.add(this.mouseOver, [this.chicken, this.chickenDance]);
        this.chicken.events.onInputDown.add(this.mouseDown, [this.chicken, this.chickenDance]);
        this.chicken.events.onInputOut.add(this.mouseOut, [this.chicken, this.chickenDance]);
        this.horse.events.onInputOver.add(this.mouseOver, [this.horse, this.horseDance]);
        this.horse.events.onInputDown.add(this.mouseDown, [this.horse, this.horseDance]);
        this.horse.events.onInputOut.add(this.mouseOut, [this.horse, this.horseDance]);
        this.sheep.events.onInputOver.add(this.mouseOver, [this.sheep, this.sheepDance]);
        this.sheep.events.onInputDown.add(this.mouseDown, [this.sheep, this.sheepDance]);
        this.sheep.events.onInputOut.add(this.mouseOut, [this.sheep, this.sheepDance]);
        this.raccoon.events.onInputOver.add(this.mouseOver, [this.raccoon, this.raccoonDance]);
        this.raccoon.events.onInputDown.add(this.mouseDown, [this.raccoon, this.raccoonDance]);
        this.raccoon.events.onInputOut.add(this.mouseOut, [this.raccoon, this.raccoonDance]);


        // this.singleLabel.events.onInputOver.add(function(){ console.log('click'); }, this.singleLabel);

        this.singleLabel = game.add.sprite(450, 320, 'singleUser');
        this.singleLabel.alpha = 0;
        this.singleLabel.scale.setTo(0.8);
        this.singleLabel.anchor.setTo(0.5, 0.5);
        this.singleLabel.events.onInputOver.add(this.overLabel, this.singleLabel);
        this.singleLabel.events.onInputOut.add(this.outLabel, this.singleLabel);
        this.singleLabel.events.onInputDown.add(this.downLabel, this.singleLabel);
        this.multipleLabel = game.add.sprite(450, 400, 'multipleUser');
        this.multipleLabel.alpha = 0;
        this.multipleLabel.scale.setTo(0.8);
        this.multipleLabel.anchor.setTo(0.5, 0.5);
        this.multipleLabel.events.onInputOver.add(this.overLabel, this.multipleLabel);
        this.multipleLabel.events.onInputOut.add(this.outLabel, this.multipleLabel);
        this.multipleLabel.events.onInputDown.add(this.downLabel, this.multipleLabel);

        this.cursor = game.add.sprite(game.input.x, game.input.y, 'cursor');

        this.selectLabel = game.add.text(0, 0, 'P1', { font: '40px Optima', fill: '#ffffff'});
        this.selectLabel.alpha = 0;
        this.selectLabel.fontWeight = 'bold';
    },
    update: function() {
        // this.explosion.play('explode', 15);
        this.cursor.x = game.input.x;
        this.cursor.y = game.input.y;
        this.selectLabel.x = game.input.x + 30;
        this.selectLabel.y = game.input.y + 5;
        if(this.enterButton.isDown){
            if(!this.enterDown){
                this.singleLabel.inputEnabled = true;
                this.multipleLabel.inputEnabled = true;
                if (isCheckedRole) {
                    isCheckedRole = false;
                    instance.post('/checkAvailableRole', {}).then(res => {
                        const { selectedPlayers } = res.data;
                        for (const role of selectedPlayers) {
                            this[role].destroy();
                        }
                    });
                }
                this.startLabel.kill();
                let explosion = this.explosion.getFirstExists(false);
                explosion.reset(320, 200);
                explosion.scale.setTo(0.9);
                explosion.play('explosion', 35, false, true);
                setTimeout(() => {
                    this.singleLabel.alpha = 1;
                    this.multipleLabel.alpha = 1;
                }, 400);
                this.enterDown = true;
            }
            
        }
    },
    selectUser: function() {
        // while(this.bgTween.isRunning) {};
        this.chicken.x = 200;
        this.chicken.y = 270;
        this.horse.x = 340;
        this.horse.y = 270;
        this.sheep.x = 515;
        this.sheep.y = 270;
        this.raccoon.x = 650;
        this.raccoon.y = 280;
        this.title.alpha = 1;
        this.selectLabel.alpha = 1;
        this.chicken.alpha = 1;
        this.horse.alpha = 1;
        this.sheep.alpha = 1;
        this.raccoon.alpha = 1;
        this.cursor.alpha = 1;
    },
    mouseOver: function(){
        if (menuState.mode == 'multi' && (game.global.whatISelected || game.global.whatOtherSelected.includes(this[0].key))) return;

        this[0].alpha = 0;
        this[1].alpha = 1;
        this[1].play('dance', 13, true, true);
    },
    mouseOut: function(){
        if (menuState.mode == 'multi' && (game.global.whatISelected || game.global.whatOtherSelected.includes(this[0].key))) return;
        this[0].alpha = 1;
        this[1].alpha = 0;
        this[1].animations.stop(null, true);
    },
    mouseDown: function() {
        if(menuState.mode == 'single') {
            game.global.character[menuState.p] = this[0].key;
            if(menuState.p == 4)    game.state.start('play');
            menuState.p += 1;
            menuState.selectLabel.text = 'P' + menuState.p;
            this[0].destroy();
            this[1].destroy();
        }
        else if(menuState.mode == 'multi') {
            if (game.global.whatISelected || game.global.whatOtherSelected.includes(this[0].key)) return;
            game.global.character[game.global.playerId] = this[0].key;
            game.global.whatISelected = this[0].key;
            // if(game.global.playerId == 4)    game.state.start('multi');
            // game.global.playerId += 1;
            // menuState.selectLabel.text = 'P' + game.global.playerId;
            socket.emit('roleDesignated', { role: this[0].key });
            this[0].destroy();
            this[1].destroy();
            menuState.cursor.alpha = 0;
            menuState.selectLabel.alpha = 0;    
        }

    },
    explosionSet: function(obj) {
        obj.anchor.x = 0;
        obj.anchor.y = 0;
        obj.animations.add('explosion');
    },
    overLabel: function() {
        console.log('over');
        game.add.tween(this.scale).to({x: 1, y: 1}, 100).start();

    },
    outLabel: function() {
        game.add.tween(this.scale).to({x: 0.8, y: 0.8}, 100).start();

    },
    downLabel: async function() {
        menuState.singleLabel.kill();
        menuState.multipleLabel.kill();
        menuState.bgTween = game.add.tween(menuState.bg).to({x: -732}, 600).start();
        menuState.bgTween.onComplete.add(function(){
            // setTimeout(function(){ menuState.select(); }, 300);
            setTimeout(function() {
                menuState.selectUser();
            }, 300);
        }, null);

        if(this.key == 'singleUser') {
            menuState.mode = 'single';
        }
        else if(this.key == 'multipleUser') {
            menuState.mode = 'multi';
            // Connect to backend
            game.global.player = {};
            // 1. Req for new player , retrieve player id (token).
            const res = await instance.post('/newPlayer', {});
            game.global.playerId = res.data.id;
            currentPlayerId = res.data.id;
            menuState.selectLabel.text = 'P' + game.global.playerId;
            // 2. Initialize socket process
            socket.emit('initialize', { id: game.global.playerId });
            socket.on('roleSelected', (d) => {
                menuState[d.role].alpha = 0;
                game.global.whatOtherSelected.push(d.role);
            });
            socket.on('toState1', async () => {
                const res = await instance.post('/getAllPlayers', {});
                for (const player of res.data.allPlayers) {
                    game.global.character[player.id] = player.role;
                }
                game.global.playerNum = 4;

                game.state.start('multi');
            });

            if (isCheckedRole) {
                isCheckedRole = false;
                instance.post('/checkAvailableRole', {}).then(res => {
                    const { selectedPlayers } = res.data;
                    for (const role of selectedPlayers) {
                        this[role].destroy();
                    }
                });
            }
        }
    }
}

window.onbeforeunload = function () {
    socket.emit('close', { playerId: currentPlayerId });
};