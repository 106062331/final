// [TODO] 將 update rate 灌到全部 socket
const UPDATE_RATE = 50;

var multiplayState = {
    preload: function() {
        // Assets.
        game.load.image('background', 'assets/maps/rooftops.png');
        game.load.image('book', 'assets/obstacles/book.png');
        game.load.image('board', 'assets/obstacles/board.png');
        game.load.image('boardCol', 'assets/obstacles/boardCol.png');
        game.load.image('box', 'assets/obstacles/box.png');
        game.load.image('metal', 'assets/obstacles/metal.png');
        game.load.image('straw', 'assets/obstacles/straw.png');
        game.load.image('thorn', 'assets/obstacles/thorn.png');
        game.load.image('leaderboard','assets/leaderboard/leaderboard.png');
        game.load.image('chickenLB','assets/leaderboard/chicken.png');
        game.load.image('horseLB','assets/leaderboard/horse.png');
        game.load.image('sheepLB','assets/leaderboard/sheep.png');
        game.load.image('raccoonLB','assets/leaderboard/raccoon.png');
        game.load.image('score','assets/leaderboard/score.png');
        game.load.image('chickenSelect','assets/characters/chickenSelect.png');
        game.load.image('horseSelect','assets/characters/horseSelect.png');
        game.load.image('sheepSelect','assets/characters/sheepSelect.png');
        game.load.image('raccoonSelect','assets/characters/raccoonSelect.png');
        game.load.spritesheet('chicken', 'assets/characters/chickenSheet.png', 130, 180);
        game.load.spritesheet('chickenDance', 'assets/characters/chickenDance.png', 130, 180);
        game.load.spritesheet('horse', 'assets/characters/horseSheet.png', 130, 180);
        game.load.spritesheet('horseDance', 'assets/characters/horseDance.png', 130, 180);
        game.load.spritesheet('sheep', 'assets/characters/sheepSheet.png', 130, 180);
        game.load.spritesheet('sheepDance', 'assets/characters/sheepDance.png', 130, 180);
        game.load.spritesheet('raccoon', 'assets/characters/raccoonSheet.png', 130, 180);
        game.load.spritesheet('raccoonDance', 'assets/characters/raccoonDance.png', 130, 180);

        // Tilemap.
        game.load.image('tileset', 'tilemaps/tileset.png');
        game.load.tilemap('map', 'tilemaps/rooftops.json', null, Phaser.Tilemap.TILED_JSON);

        // Audio.
        game.load.audio('music', 'assets/audio/play.mp3');
    },
    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.add.image(0, 0, 'background');

        // Variable initialize
        this.isObstacleSelected = false;
        this.isSelfReselected = false;

        // Tilemap.
        this.map = game.add.tilemap('map');
        this.map.addTilesetImage('tileset');
        this.layer = this.map.createLayer('Layer1');
        this.layer.resizeWorld();
        this.map.setCollision(1);
        this.layer.alpha = 0;
        
        // Obstacles.
        this.obstacles = game.add.group();
        this.obstacles.enableBody = true;
        this.curObstacle = 'board';
        
        // Initialize repository.
        this.repository = game.add.sprite(0, 0, 'book');
        this.repository.scale.setTo(0.5, 0.5);
        this.items = game.add.group();
        this.itemName = ['board', 'boardCol', 'box', 'metal', 'straw', 'thorn'];
        for(var i=0;i<6;i++){
            this.item = this.items.create(-200, -200, this.itemName[i]);
            this.item.scale.setTo(0.5, 0.5);
            this.item.anchor.setTo(0.5, 0.5);
            this.item.inputEnabled = true;
        }
        this.createRepository();
        // For each item, if user clicks it, change state.
        this.items.forEach((item) => {
            item.events.onInputDown.add(() => {
                if(multiplayState.gameState == 1){
                    multiplayState.curObstacle = item.key;
                    multiplayState.selectedObstacle = game.add.sprite(0, 0, item.key);
                    game.physics.arcade.enable(multiplayState.selectedObstacle);
                    multiplayState.selectedObstacle.scale.setTo(0.5, 0.5);
                    // multiplayState.gameState = 2;
                    multiplayState.isObstacleSelected = true;
                    multiplayState.items.setAll('alpha', 0);
                    multiplayState.repository.alpha = 0;

                    // socket connection
                    socket.emit('selectedObstacle', { playerId: multiplayState.nowPlayer, key: item.key });
                }
            }, this);
        });

        // Player.
        this.player = [];
        this.nowPlayer = game.global.playerId;
        for(var i=1;i<=game.global.playerNum;i++){
          this.player[i] = game.add.sprite(600, 700, game.global.character[i]);
          this.player[i].scale.setTo(0.45, 0.45);
          this.player[i].anchor.setTo(0.5);
          game.physics.arcade.enable(this.player[i]);
          this.player[i].body.gravity.y = 1200;
          this.player[i].outOfBoundsKill = true;
          this.player[i].checkWorldBounds = true;
          this.player[i].facingLeft = false;
          this.player[i].isDead = false;
          this.player[i].animations.add('rightwalk', [ 2, 3 ], 10, true);
          this.player[i].animations.add('leftwalk', [ 4, 5 ], 10, true);
          this.player[i].animations.add('dead', [ 14, 14, 15, 16 ], 5, false);
          this.player[i].score = 0;
          this.player[i].isArrivedGoal = false;
        //   this.player[i].dance = null;
            this.player[i].dance = game.add.sprite(0, 0, game.global.character[i]+'Dance');
            this.player[i].dance.scale.setTo(0.55, 0.55);
            this.player[i].dance.anchor.setTo(0.5);
            this.player[i].dance.animations.add('dance');
            this.player[i].dance.play('dance', 10, true, true);
            this.player[i].dance.alpha = 0;
          this.player[i].kill();
        }
        
        // Leaderboard.
        this.leaderboard = game.add.sprite(0,0,'leaderboard');
        this.leaderboard.scale.setTo(0.75,0.75);
        this.leaderboard.kill();

        this.leaderboard.players = [];
        for(var i=1;i<=game.global.playerNum;i++){
        this.leaderboard.players[i] = game.add.sprite(0,0,game.global.character[i]+'LB');
        this.leaderboard.players[i].scale.setTo(0.75,0.75);
        this.leaderboard.players[i].kill();
        }
        
        // ScoreBar.
        this.scoreBar = game.add.group();
        this.scoreBar.createMultiple(30,'score');
        this.scoreBar.setAll('anchor.x',0);
        this.scoreBar.setAll('anchor.y',0.5);
        this.scoreBar.setAll('scale.x',0.75);
        this.scoreBar.setAll('scale.y',0.75);
        this.scoreTest = game.add.sprite(-300,-300,'score');
        this.crop = [];
        this.raisingScore = [];
        this.tween = [];
        
        // Game states.
        // (1) Choose obstacle -> (2) Build the level -> (3) Run to the end -> (4) Leaderboard.
        this.gameState = -1;
        game.camera.x = 300;
        game.camera.y = 600;
        setTimeout(()=>{
            this.changeState(1);
        }, 100);

        this.cursor = game.input.keyboard.createCursorKeys();
        this.mouseCursor = game.add.sprite(game.camera.x, game.camera.y, game.global.character[this.nowPlayer]+'Select');
        this.mouseCursor.scale.setTo(0.5, 0.5);
        //flag
        this.changeStateFlag = true;
        // Background music.
        this.music = game.add.audio('music', 1, true);
        this.music.play();

        // this.player.forEach((element) => {
        //     element.events.onKilled.add(() => {
        //         for(var i=1;i<=game.global.playerNum;i++){
        //             if(this.player[i].alive){
        //                 break;
        //             }
        //             if(i == game.global.playerNum && this.changeStateFlag){
        //                 this.changeStateFlag = false;
        //                 this.changeState(4);
        //             }
        //         }
        //     }, this);
        // });
        this.player[this.nowPlayer].events.onKilled.add(() => {
            if (this.gameState !== 3) return;
            socket.emit('playerStatusChange', {
                playerId: this.nowPlayer,
                status: this.player[this.nowPlayer].isArrivedGoal ? 'goal' : 'dead'
            });
        }, this);

        socket.on('allPlayerStatusSet', (_players) => {
            if (this.gameState !== 3) return;
            for(const _player of _players) {
                if (_player.status === 'goal') this.player[_player.playerId].score++;
            }

            this.changeState(4);
        });

        /* Backend connection */
        socket.on('toState2', () => {
            console.log('Received state 2 change request.');
            this.changeState(2);
        });
        // state 2
        socket.on('receiveObstacle', (data) => {
            const { posX, posY, obstacleType, playerId } = data;
            this.createObstacle(posX, posY, obstacleType);
            this.otherObstacles[playerId].destroy();
            this.otherMouseCursor[playerId].destroy();
        });
        socket.on('toState3', () => {
            this.changeState(3);
        });
        this.otherObstacles = {
            1: null,
            2: null,
            3: null,
            4: null
        };
        this.otherMouseCursor = {
            1: null,
            2: null,
            3: null,
            4: null
        }
        socket.on('otherObstacleCreate', (data) => {
            const newObs = game.add.sprite(0, 0, data.key);
            game.physics.arcade.enable(newObs);
            newObs.scale.setTo(0.5, 0.5);
            this.otherObstacles[data.playerId] = newObs;
        });
        socket.on('otherObstacleMove', (data) => {
            if (this.gameState !== 2) return;
            if (!this.otherMouseCursor[data.playerId]) {
                this.otherMouseCursor[data.playerId] = game.add.sprite(0, 0, game.global.character[data.playerId]+'Select');
                this.otherMouseCursor[data.playerId].scale.setTo(0.5, 0.5);
            }
            this.otherMouseCursor[data.playerId].x = data.x - 160;
            this.otherMouseCursor[data.playerId].y = data.y - 60;
            this.otherObstacles[data.playerId].x = data.x;
            this.otherObstacles[data.playerId].y = data.y;
        });
        // state 4
        socket.on('finishScoreDisplay', () => {
            this.scoreFlag = false;
            this.leaderboard.kill();
            for(var i=1;i<=game.global.playerNum;i++){
                this.leaderboard.players[i].kill();
            }
            this.scoreBar.forEachAlive(score=>{
                score.kill();
            })
            for(var i=1;i<=game.global.playerNum;i++){
                if(this.player[i].isArrivedGoal){
                    this.tween[i].stop();
                }
            }
            for(var i=1;i<=game.global.playerNum;i++){
                if(this.player[i].score==5){
                    this.scoreFlag = true;
                    this.changeState(5);
                    break;
                }
            }
            if(!this.scoreFlag){
                this.changeStateFlag = true;
                this.changeState(1);
            }
        });
        // reset
        socket.on('toMenu', () => {
            game.state.start('menu');
        })

    },
    update: function() {
        // Mouse cursor.
        this.mouseCursor.x = game.camera.x + game.input.x - 160;
        this.mouseCursor.y = game.camera.y + game.input.y - 60;
        // Game state handler.
        switch(this.gameState){
            case 1:
                if (!this.isObstacleSelected) {
                    this.items.forEach(function(item){
                        if (item.input.pointerOver()){
                            item.alpha = 1;
                            game.add.tween(item.scale).to({x: 0.55, y: 0.55}, 50).start();
                        }
                        else{
                            item.alpha = 0.9;
                            game.add.tween(item.scale).to({x: 0.5, y: 0.5}, 50).start();
                        }
                    });
                }
                else {
                    // 如果是自己重選的，選完自己跳 state 2
                    if (this.isSelfReselected) {
                        this.changeState(2);
                        this.isSelfReselected = false;
                    }
                    this.mouseCursor.alpha = 0;
                }
            break;
            case 2:
                // Scroll map via cursor.
                this.mouseCursor.alpha = 1;
                this.scrollMap();

                this.pos_x = Math.round((game.camera.x + game.input.x)/40)*40;
                this.pos_y = Math.round((game.camera.y + game.input.y)/40)*40;

                this.selectedObstacle.x = this.pos_x;
                this.selectedObstacle.y = this.pos_y;

                socket.emit('obstacleMove', {
                    playerId: this.nowPlayer,
                    x: this.pos_x,
                    y: this.pos_y
                })

                // Overlaps.
                this.selectedObstacle.isOverlap = false;
                game.physics.arcade.overlap(this.selectedObstacle , this.obstacles, () => { this.selectedObstacle.isOverlap = true; }, null, this);

                if(this.selectedObstacle.isOverlap){
                    this.selectedObstacle.tint = 0xff0000;
                    this.selectedObstacle.alpha = 0.5;
                } else {
                    this.selectedObstacle.tint = 0xffffff;
                    this.selectedObstacle.alpha = 1;

                    // Press Spacebar to create obstacle.
                    if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
                        this.selectedObstacle.destroy();
                        this.createObstacle(this.pos_x, this.pos_y, this.curObstacle);

                        socket.emit('createObstacle', {
                            playerId: this.nowPlayer,
                            posX: this.pos_x,
                            posY: this.pos_y,
                            obstacleType: this.curObstacle
                        });
                        
                        // Change next player.
                        // if(this.nowPlayer == game.global.playerNum){
                        //     this.nowPlayer = 1;
                        //     this.changeState(3);
                        // }
                        // else{
                        //     this.nowPlayer++;
                        //     this.changeState(1);
                        // }
                        this.mouseCursor.loadTexture(game.global.character[this.nowPlayer]+'Select', 0, false);
                    }
                    
                    // Press R to re-choose item.
                    if(game.input.keyboard.isDown(Phaser.Keyboard.R)){
                        this.changeState(1);
                        this.isSelfReselected = true;
                        this.selectedObstacle.destroy();
                    }
                }
            break;
            case 3:
                // Collisions.
                for(var i=1;i<=game.global.playerNum;i++){
                  game.physics.arcade.collide(this.player[i] , this.layer);
                  game.physics.arcade.collide(this.player[i], this.obstacles, this.collideObstacle, null, this);
                }
                
                this.controlPlayer(this.player[this.nowPlayer]);
                socket.emit('move', {
                    playerId: this.nowPlayer,
                    x: this.player[this.nowPlayer].x,
                    y: this.player[this.nowPlayer].y,
                    frame: this.player[this.nowPlayer].frame
                });
                socket.on('syncMove', (data) => {
                    this.player[data.id].x = data.x;
                    this.player[data.id].y = data.y;
                    this.player[data.id].frame = data.frame;
                });


                // Press B to give up.
                if (game.input.keyboard.isDown(Phaser.Keyboard.B)) {
                    // [TODO] 其他人 failed
                    this.killPlayer(this.player[this.nowPlayer]);
                }
  
                // Check if the player reaches goal.
                for(let i=1;i<=game.global.playerNum;i++){
                    if(this.player[i].x > 2600 && this.player[i].x < 2800 && this.player[i].y > 700 && this.player[i].y < 850){
                        this.arriveGoal(this.player[i]);
                    }
                }
                // // If the player is dead or reaches goal, start a new round.
                // if(!this.player.alive && !this.player.isArrivedGoal[this.nowPlayer]){
                //     this.changeNextPlayer();
                // }
            break;
            case 4:
                for(var i=1;i<=game.global.playerNum;i++){
                    if(this.player[i].isArrivedGoal){
                        this.raisingScore[i].updateCrop();
                    }
                }
            break;
            case 5:
                if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
                    game.state.start('menu');
                }
            break;
        }
    },
    // changeNextPlayer: function(){
    //     if(this.nowPlayer == game.global.playerNum){
    //         this.nowPlayer = 1;
    //         this.changeState(4);
    //     }
    //     else{
    //         this.nowPlayer++;
    //         this.resetPlayer();
    //     }
    //     this.player.loadTexture(game.global.character[this.nowPlayer],0,false);
    // },
    createObstacle: function(x, y, name){
        this.obstacle = this.obstacles.create(x, y, name);
        this.obstacle.scale.setTo(0.5, 0.5);
        this.obstacle.body.immovable = true;
        // this.resetPlayer();
    },
    killPlayer: function(player){
        if(player.isDead) return;

        player.isDead = true;
        player.animations.play('dead');

        setTimeout(()=>{
            player.kill();
        }, 2000);
    },
    controlPlayer: function(player){
        // Set player's acceleration to 0.
        player.body.acceleration.x = 0;

        if(!player.isDead && !player.isArrivedGoal){
            // Player move.
            if(this.cursor.left.isDown && !this.cursor.right.isDown){
                player.body.acceleration.x -= 2000;
                player.facingLeft = true;
                if(player.body.blocked.down || player.body.touching.down){
                    player.animations.play('leftwalk');
                } else {
                    if(player.body.velocity.y < 0) player.frame = 7;
                    else player.frame = 9;
                }
            }
            else if(this.cursor.right.isDown && !this.cursor.left.isDown){
                player.body.acceleration.x += 2000;
                player.facingLeft = false;
                if(player.body.blocked.down || player.body.touching.down){
                    player.animations.play('rightwalk');
                } else {
                    if(player.body.velocity.y < 0) player.frame = 6;
                    else player.frame = 8;
                }
            }
            else {
                player.body.velocity.x /= 1.4;
                if(player.body.blocked.down || player.body.touching.down){
                    if(player.facingLeft) player.frame = 1;
                    else player.frame = 0;
                } else {
                    if(player.facingLeft){
                        if(player.body.velocity.y < 0) player.frame = 7;
                        else player.frame = 9;
                    }
                    else {
                        if(player.body.velocity.y < 0) player.frame = 6;
                        else player.frame = 8;
                    }
                }
            }

            // Limit the max speed of the player.
            if(player.body.velocity.x > 400) player.body.velocity.x = 400;
            if(player.body.velocity.x < -400) player.body.velocity.x = -400;

            // Check if the player is touching the wall, if yes, give him a friction.
            if(player.body.blocked.left || player.body.blocked.right || player.body.touching.left || player.body.touching.right){
                if(player.body.velocity.y > 200) player.body.velocity.y = 200;
            }

            // Player jump.
            if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
                if(player.body.blocked.down || player.body.touching.down){
                    player.body.velocity.y = -400;
                } else {
                    if(player.body.blocked.left || player.body.touching.left){
                        player.body.velocity.x = 800;
                        player.body.velocity.y = -400;
                    }
                    else if(player.body.blocked.right || player.body.touching.right){
                        player.body.velocity.x = -800;
                        player.body.velocity.y = -400;
                    }
                }
            }

            // Animations when the player is against the wall.
            if(!player.body.blocked.down && !player.body.touching.down){
                if(player.body.blocked.left || player.body.touching.left){
                    player.frame = 12;
                }
                else if(player.body.blocked.right || player.body.touching.right){
                    player.frame = 13;
                }
            }
        } else {
            // Player is dead.
            player.body.velocity.x = 0;
        }
    },
    arriveGoal: function(player){
      if(player.isArrivedGoal) return;
      
      player.dance.reset(player.x, player.y);
      player.dance.alpha = 1;

    //   if(player.score < 5){
    //       player.score++;
    //   };

      player.isArrivedGoal = true;
      player.kill();
      
    },
    resetPlayer: function(player){
        player.facingLeft = false;
        player.isDead = false;
        player.reset(600, 700);
    },
    scrollMap: function(){
        if (game.input.y < 100 || this.cursor.up.isDown) game.camera.y -= 10;
        else if (game.input.y > game.height - 100 || this.cursor.down.isDown) game.camera.y += 10;
    
        if (game.input.x < 100 || this.cursor.left.isDown) game.camera.x -= 10;
        else if (game.input.x > game.width - 100 || this.cursor.right.isDown) game.camera.x += 10;
    },
    changeState: function(state){
        this.gameState = state;
        switch(state){
            case 1:
                for(var i=1;i<=game.global.playerNum;i++){
                  if(this.player[i].alive) this.player[i].kill();
                }
                game.camera.follow(null);
                this.isObstacleSelected = false;
                this.createRepository();
                this.mouseCursor.alpha = 1;
            break;
            case 2:
                for(var i=1;i<=game.global.playerNum;i++){
                  if(this.player[i].alive) this.player[i].kill();
                }
                game.camera.follow(null);
            break;
            case 3:
                for (const cursor in this.otherMouseCursor) {
                    if (this.otherMouseCursor[cursor]) {
                        this.otherMouseCursor[cursor].destroy();
                        this.otherMouseCursor[cursor] = null;
                    }
                }
                for (const obstacle in this.otherObstacles) {
                    if (this.otherObstacles[obstacle]) {
                        this.otherObstacles[obstacle].destroy();
                        this.otherObstacles[obstacle] = null;
                    }
                }

                for(var i=1;i<=game.global.playerNum;i++){
                    this.player[i].isArrivedGoal = false;
                }
                // this.nowPlayer = 1;
                for(var i=1;i<=game.global.playerNum;i++){
                  this.resetPlayer(this.player[i]);
                }
                game.camera.follow(this.player[this.nowPlayer]);
                this.mouseCursor.alpha = 0;
            break;
            case 4:
              for(var i=1;i<=game.global.playerNum;i++){
                  this.player[i].dance.alpha = 0;
              }
              this.createLeaderboard();
              for(var i=1;i<=game.global.playerNum;i++){
                  if(this.player[i].isArrivedGoal){
                      this.tween[i].start();
                  }
              }
              socket.emit('scoreDisplay', { playerId: this.nowPlayer });
            break;
            case 5:
                game.camera.follow(null);
                game.camera.x = 2120;
                game.camera.y = 569;
                var dance = [];
                for(var i=1, j=0;i<=game.global.playerNum;i++){
                    if(this.player[i].score == 5){
                        dance[i] = game.add.sprite(game.camera.x + 480 + j * 100,game.camera.y + 271, game.global.character[i]+'Dance');
                        dance[i].scale.setTo(0.55, 0.55);
                        dance[i].anchor.setTo(0.5);
                        dance[i].animations.add('dance');
                        dance[i].play('dance', 10, true, true);
                        j++;
                    }
                }
            break;
        }
    },
    createRepository: function(){
        let offsetX = 0;
        let offsetY = 0;
        this.items.forEach(function(item){
            switch(item.key){
                case 'board':
                    offsetX = 160;
                    offsetY = 80;
                break;
                case 'boardCol':
                    offsetX = 80;
                    offsetY = 160;
                break;
                case 'box':
                    offsetX = 120;
                    offsetY = 120;
                break;
                case 'metal':
                    offsetX = 180;
                    offsetY = 140;
                break;
                case 'straw':
                    offsetX = 280;
                    offsetY = 160;
                break;
                case 'thorn':
                    offsetX = 120;
                    offsetY = 160;
                break;
                default:
                    offsetX = 500;
                    offsetY = 500;
                break;
            }
            item.reset(game.camera.x + offsetX - 800, game.camera.y + offsetY);
            game.add.tween(item).to({x: game.camera.x + offsetX}, 1000).start();
        });
        this.repository.reset(game.camera.x - 800, game.camera.y);
        this.repository.alpha = 1;
        game.add.tween(this.repository).to({x: game.camera.x}, 1000).start();
    },
    collideObstacle: function(player, item){
        if(item.key === 'thorn'){
            this.killPlayer(player);
        }
    },
    createLeaderboard: function(){
        /*
        x = game.camera.x+218 ->first
        x = game.camera.x+326 ->second
        */
        this.leaderboard.reset(game.camera.x-55,game.camera.y-35);
        for(var i=1;i<=game.global.playerNum;i++){
          this.leaderboard.players[i].reset(game.camera.x+30,game.camera.y+90*i);
        }
      for(var j=1;j<=game.global.playerNum;j++){
        if(this.player[j].isArrivedGoal){
          for(var i=0;i<this.player[j].score-1;i++){
            var score = this.scoreBar.getFirstExists(false);
            score.reset(game.camera.x+218+108*i,game.camera.y+150+(j-1)*87);
          }
          
          this.crop[j] = new Phaser.Rectangle(0,0,0,this.scoreTest.height);
          this.raisingScore[j] = this.scoreBar.getFirstExists(false);
          this.raisingScore[j].reset(game.camera.x+218+108*(this.player[j].score-1),game.camera.y+150+(j-1)*87);
          this.tween[j] = game.add.tween(this.crop[j]).to({width:this.scoreTest.width},600);
          this.raisingScore[j].crop(this.crop[j]);
        }
        else{
          for(var i=0;i<this.player[j].score;i++){
            var score = this.scoreBar.getFirstExists(false);
            score.reset(game.camera.x+218+108*i,game.camera.y+150+(j-1)*87);
          }
        }
      }

    }
}

window['multiplayState'] = multiplayState;