var charactersState = {
    preload: function(){
        // var canvas = document.getElementsByTagName('canvas')[0];
        // canvas.style.cursor = "url('assets/cursor.png'), auto";
        // game.canvas.style.cursor = "url('assets/cursor.png'), auto";
        game.canvas.style.cursor = "none";
        game.load.image('bg', 'assets/others/charactersBG.png');
        game.load.image('title', 'assets/others/charTitle.png');
        game.load.image('cursor', 'assets/others/cursor.png');
        game.load.image('chicken', 'assets/characters/chicken.png');
        game.load.image('horse', 'assets/characters/horse.png');
        game.load.spritesheet('chickenDance', 'assets/characters/chickenDance.png', 130, 180);
        game.load.spritesheet('horseDance', 'assets/characters/horseDance.png', 130, 180);
    },
    create: function(){
        game.stage.backgroundColor = '#000000';
        this.bg = game.add.image(0, 0, 'bg');
        this.title = game.add.image(0, 0, 'title');
        this.title.anchor.setTo(0.5, 0);
        this.title.reset(game.width/2, 0);
        this.chicken = game.add.sprite(200, 250, 'chicken');
        this.chicken.loadTexture('chicken');
        this.chicken.scale.setTo(0.75);
        this.chicken.inputEnabled = true;
        this.horse = game.add.sprite(600, 250, 'horse');
        this.horse.scale.setTo(0.75);
        this.horse.inputEnabled = true;
        this.chicken.events.onInputOver.add(this.mouseOver, this.chicken);
        this.chicken.events.onInputDown.add(this.mouseDown, this.chicken);
        this.chicken.events.onInputOut.add(this.mouseOut, this.chicken);
        this.horse.events.onInputOver.add(this.mouseOver, this.horse);
        this.horse.events.onInputDown.add(this.mouseDown, this.horse);
        this.horse.events.onInputOut.add(this.mouseOut, this.horse);
        this.cursor = game.add.sprite(game.input.x, game.input.y, 'cursor');
    },
    update: function(){
        this.cursor.x = game.input.x;
        this.cursor.y = game.input.y;
    },
    mouseOver: function(){
        if(this.key == 'chicken'){
            this.loadTexture('chickenDance');
            this.reset(140, 170);
            this.scale.setTo(1.8);
        }
        else if(this.key == 'horse'){
            this.loadTexture('horseDance');
            this.reset(590, 220);
            this.scale.setTo(1.5);
        }
        this.animations.add('dance');
        this.play('dance', 13, true, true);
    },
    mouseOut: function(){
        if(this.key == 'chickenDance'){
            this.loadTexture('chicken');
            this.reset(200, 250);
        }
        else if(this.key == 'horseDance'){
            this.loadTexture('horse');
            this.reset(600, 250);
        }
        this.scale.setTo(0.75);
        this.animations.stop(null, true);
    },
    mouseDown: function() {
        if(this.key == 'chickenDance')      game.global.character = 'chicken';
        else if(this.key == 'horseDance')   game.global.character = 'horse';
        game.state.start('play');
    }
}