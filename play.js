var playState = {
    preload: function() {
        // Assets.
        game.load.image('background', 'assets/maps/rooftops.png');
        game.load.image('book', 'assets/obstacles/book.png');
        game.load.image('board', 'assets/obstacles/board.png');
        game.load.image('boardCol', 'assets/obstacles/boardCol.png');
        game.load.image('box', 'assets/obstacles/box.png');
        game.load.image('metal', 'assets/obstacles/metal.png');
        game.load.image('straw', 'assets/obstacles/straw.png');
        game.load.image('thorn', 'assets/obstacles/thorn.png');
        game.load.image('ironStructure', 'assets/obstacles/ironStructure.png');
        game.load.image('ironStructure1', 'assets/obstacles/ironStructure1.png');
        game.load.image('ironStructure2', 'assets/obstacles/ironStructure2.png');
        game.load.image('cannon', 'assets/obstacles/cannon.png');
        game.load.image('bullet', 'assets/obstacles/bullet.png');
        game.load.image('bow_top', 'assets/obstacles/bow_top.png');
        game.load.image('bow_bot', 'assets/obstacles/bow_bot.png');
        game.load.image('bow_left', 'assets/obstacles/bow_left.png');
        game.load.image('bow_right', 'assets/obstacles/bow_right.png');
        game.load.image('arrow_top', 'assets/obstacles/arrow_top.png');
        game.load.image('arrow_right', 'assets/obstacles/arrow_right.png');
        game.load.image('arrow_bot', 'assets/obstacles/arrow_bot.png');
        game.load.image('arrow_left', 'assets/obstacles/arrow_left.png');
        game.load.image('leaderboard','assets/leaderboard/leaderboard.png');
        game.load.image('chickenLB','assets/leaderboard/chicken.png');
        game.load.image('horseLB','assets/leaderboard/horse.png');
        game.load.image('sheepLB','assets/leaderboard/sheep.png');
        game.load.image('raccoonLB','assets/leaderboard/raccoon.png');
        game.load.image('score','assets/leaderboard/score.png');
        game.load.image('chickenSelect','assets/characters/chickenSelect.png');
        game.load.image('horseSelect','assets/characters/horseSelect.png');
        game.load.image('sheepSelect','assets/characters/sheepSelect.png');
        game.load.image('raccoonSelect','assets/characters/raccoonSelect.png');
        game.load.spritesheet('chicken', 'assets/characters/chickenSheet.png', 130, 180);
        game.load.spritesheet('chickenDance', 'assets/characters/chickenDance.png', 130, 180);
        game.load.spritesheet('horse', 'assets/characters/horseSheet.png', 130, 180);
        game.load.spritesheet('horseDance', 'assets/characters/horseDance.png', 130, 180);
        game.load.spritesheet('sheep', 'assets/characters/sheepSheet.png', 130, 180);
        game.load.spritesheet('sheepDance', 'assets/characters/sheepDance.png', 130, 180);
        game.load.spritesheet('raccoon', 'assets/characters/raccoonSheet.png', 130, 180);
        game.load.spritesheet('raccoonDance', 'assets/characters/raccoonDance.png', 130, 180);
        game.load.spritesheet('explode', 'assets/obstacles/explode.png', 128, 128);

        // Tilemap.
        game.load.image('tileset', 'tilemaps/tileset.png');
        game.load.tilemap('map', 'tilemaps/rooftops.json', null, Phaser.Tilemap.TILED_JSON);

        // Audio.
        game.load.audio('music', 'assets/audio/play.mp3');
    },
    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.add.image(0, 0, 'background');

        // Tilemap.
        this.map = game.add.tilemap('map');
        this.map.addTilesetImage('tileset');
        this.layer = this.map.createLayer('Layer1');
        this.layer.resizeWorld();
        this.map.setCollision(1);
        this.layer.alpha = 0;
        
        // Obstacles.
        this.obstacles = game.add.group();
        this.obstacles.enableBody = true;
        this.curObstacle = 'board';
        
        // Bullets.
        this.cannons = game.add.group();
        this.cannons.enableBody = true;
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(10, 'bullet');
        this.bullets.setAll('anchor.x', 0.4);
        this.bullets.setAll('anchor.y', 0.4);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.nextShotAt = 0;

        // Arrows.
        this.bows = game.add.group();
        this.bows.enableBody = true;
        //top:0 left:1 bot:2 right:3
        this.bows.setAll('dir',-1);
        //top
        this.arrows = game.add.group();
        this.arrows.enableBody = true;
        this.arrows.createMultiple(20, 'arrow_top');
        this.arrows.setAll('anchor.x', 0.4);
        this.arrows.setAll('anchor.y', 0.4);
        this.arrows.setAll('outOfBoundsKill', true);
        this.arrows.setAll('checkWorldBounds', true);
        //left
        this.arrows_left = game.add.group();
        this.arrows_left.enableBody = true;
        this.arrows_left.createMultiple(20, 'arrow_left');
        this.arrows_left.setAll('anchor.x', 0.4);
        this.arrows_left.setAll('anchor.y', 0.4);
        this.arrows_left.setAll('outOfBoundsKill', true);
        this.arrows_left.setAll('checkWorldBounds', true);
        //bot
        this.arrows_bot = game.add.group();
        this.arrows_bot.enableBody = true;
        this.arrows_bot.createMultiple(20, 'arrow_bot');
        this.arrows_bot.setAll('anchor.x', 0.4);
        this.arrows_bot.setAll('anchor.y', 0.4);
        this.arrows_bot.setAll('outOfBoundsKill', true);
        this.arrows_bot.setAll('checkWorldBounds', true);
        //right
        this.arrows_right = game.add.group();
        this.arrows_right.enableBody = true;
        this.arrows_right.createMultiple(20, 'arrow_right');
        this.arrows_right.setAll('anchor.x', 0.4);
        this.arrows_right.setAll('anchor.y', 0.4);
        this.arrows_right.setAll('outOfBoundsKill', true);
        this.arrows_right.setAll('checkWorldBounds', true);
        this.nextShotAt2 = 0;

        // Initialize repository.
        this.repository = game.add.sprite(0, 0, 'book');
        this.repository.scale.setTo(0.5, 0.5);
        this.items = game.add.group();
        this.itemName = ['board', 'boardCol', 'box', 'metal', 'straw', 'thorn', 'ironStructure', 'cannon', 'bow_top', 'bow_left', 'bow_bot', 'bow_right'];
        for(var i=0;i<12;i++){
            this.item = this.items.create(-200, -200, this.itemName[i]);
            this.item.scale.setTo(0.5, 0.5);
            this.item.anchor.setTo(0.5, 0.5);
            this.item.inputEnabled = true;
            this.item.input.pixelPerfectOver = true;
        }
        this.createRepository();
        // For each item, if user clicks it, change state.
        this.items.forEach(function(item){
            item.events.onInputDown.add(() => {
                if(playState.gameState == 1){
                    playState.curObstacle = item.key;
                    playState.selectedObstacle = game.add.sprite(0, 0, item.key);
                    game.physics.arcade.enable(playState.selectedObstacle);
                    playState.selectedObstacle.scale.setTo(0.5, 0.5);
                    playState.gameState = 2;
                    playState.items.setAll('alpha', 0);
                    playState.repository.alpha = 0;
                }
            }, this);
        });

        // Player.
        this.nowPlayer = 1;
        this.player = game.add.sprite(600, 700, game.global.character[1]);
        this.player.scale.setTo(0.45, 0.45);
        this.player.anchor.setTo(0.5);
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 1200;
        this.player.outOfBoundsKill = true;
        this.player.checkWorldBounds = true;
        this.player.facingLeft = false;
        this.player.isDead = false;
        this.player.animations.add('rightwalk', [ 2, 3 ], 10, true);
        this.player.animations.add('leftwalk', [ 4, 5 ], 10, true);
        this.player.animations.add('dead', [ 14, 14, 15, 16 ], 5, false);
        this.player.score = [];
        this.player.isArrivedGoal = [];
        for(var i = 1;i<=game.global.playerNum;i++){
            this.player.isArrivedGoal[i] = false;
            this.player.score[i] = 0;
        }
        this.player.kill();
        
        // Leaderboard.
        this.leaderboard = game.add.sprite(0,0,'leaderboard');
        this.leaderboard.scale.setTo(0.75,0.75);
        this.leaderboard.kill();

        this.leaderboard.players = [];
        for(var i=1;i<=game.global.playerNum;i++){
        this.leaderboard.players[i] = game.add.sprite(0,0,game.global.character[i]+'LB');
        this.leaderboard.players[i].scale.setTo(0.75,0.75);
        this.leaderboard.players[i].kill();
        }
        
        // ScoreBar.
        this.scoreBar = game.add.group();
        this.scoreBar.createMultiple(30,'score');
        this.scoreBar.setAll('anchor.x',0);
        this.scoreBar.setAll('anchor.y',0.5);
        this.scoreBar.setAll('scale.x',0.75);
        this.scoreBar.setAll('scale.y',0.75);
        this.scoreTest = game.add.sprite(-300,-300,'score');
        this.crop = [];
        this.raisingScore = [];
        this.tween = [];

        // Game states.
        // (1) Choose obstacle -> (2) Build the level -> (3) Run to the end -> (4) Leaderboard.
        this.gameState = -1;
        game.camera.x = 300;
        game.camera.y = 600;
        setTimeout(()=>{
            this.changeState(1);
        }, 100);

        this.cursor = game.input.keyboard.createCursorKeys();
        this.mouseCursor = game.add.sprite(game.camera.x, game.camera.y, game.global.character[this.nowPlayer]+'Select');
        this.mouseCursor.scale.setTo(0.5, 0.5);

        // Background music.
        this.music = game.add.audio('music', 1, true);
        this.music.play();
    },
    update: function() {
        // Mouse cursor.
        this.mouseCursor.x = game.camera.x + game.input.x - 160;
        this.mouseCursor.y = game.camera.y + game.input.y - 60;
        // Game state handler.
        switch(this.gameState){
            case 1:
                this.items.forEach(function(item){
                    if (item.input.pointerOver()){
                        item.alpha = 1;
                        game.add.tween(item.scale).to({x: 0.55, y: 0.55}, 50).start();
                    }
                    else{
                        item.alpha = 0.9;
                        game.add.tween(item.scale).to({x: 0.5, y: 0.5}, 50).start();
                    }
                });
            break;
            case 2:
                // Scroll map via cursor.
                this.scrollMap();

                this.pos_x = Math.round((game.camera.x + game.input.x)/40)*40;
                this.pos_y = Math.round((game.camera.y + game.input.y)/40)*40;

                this.selectedObstacle.x = this.pos_x;
                this.selectedObstacle.y = this.pos_y;

                // Overlaps.
                this.selectedObstacle.isOverlap = false;
                game.physics.arcade.overlap(this.selectedObstacle , this.obstacles, () => { this.selectedObstacle.isOverlap = true; }, null, this);
                game.physics.arcade.overlap(this.selectedObstacle , this.cannons, () => { this.selectedObstacle.isOverlap = true; }, null, this);
                game.physics.arcade.overlap(this.selectedObstacle , this.bows, () => { this.selectedObstacle.isOverlap = true; }, null, this);

                if(this.selectedObstacle.isOverlap){
                    this.selectedObstacle.tint = 0xff0000;
                    this.selectedObstacle.alpha = 0.5;
                } else {
                    this.selectedObstacle.tint = 0xffffff;
                    this.selectedObstacle.alpha = 1;

                    // Press Spacebar to create obstacle.
                    if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
                        this.selectedObstacle.destroy();
                        this.createObstacle(this.pos_x, this.pos_y, this.curObstacle);
                        
                        // Change next player.
                        if(this.nowPlayer == game.global.playerNum){
                            this.nowPlayer = 1;
                            this.changeState(3);
                        }
                        else{
                            this.nowPlayer++;
                            this.changeState(1);
                        }
                        this.mouseCursor.loadTexture(game.global.character[this.nowPlayer]+'Select', 0, false);
                    }
                    
                    // Press R to re-choose item.
                    if(game.input.keyboard.isDown(Phaser.Keyboard.R)){
                        this.changeState(1);
                        this.selectedObstacle.destroy();
                    }
                }
            break;
            case 3:
                // Collisions.
                game.physics.arcade.collide(this.player , this.layer);
                game.physics.arcade.collide(this.player, this.obstacles, this.collideObstacle, null, this);
                game.physics.arcade.collide(this.player, this.cannons);
                game.physics.arcade.collide(this.player, this.bows);
                game.physics.arcade.overlap(this.player, this.bullets, this.bulletHitPlayer, null, this);
                game.physics.arcade.overlap(this.player, this.arrows, this.bulletHitPlayer, null, this);
                game.physics.arcade.overlap(this.player, this.arrows_left, this.bulletHitPlayer, null, this);
                game.physics.arcade.overlap(this.player, this.arrows_bot, this.bulletHitPlayer, null, this);
                game.physics.arcade.overlap(this.player, this.arrows_right, this.bulletHitPlayer, null, this);
                game.physics.arcade.collide(this.bullets, this.layer, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.bullets, this.obstacles, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows, this.layer, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows, this.obstacles, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows_left, this.layer, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows_left, this.obstacles, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows_right, this.layer, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows_right, this.obstacles, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows_bot, this.layer, this.bulletHitGround, null, this);
                game.physics.arcade.collide(this.arrows_bot, this.obstacles, this.bulletHitGround, null, this);
                
                //DeadException
                if(this.player.isDead){
                    this.player.outOfBoundsKill = false;
                }
                else{
                    this.player.outOfBoundsKill = true;
                }

                // Cannons.
                if(game.time.now > this.nextShotAt){
                    this.cannons.forEachAlive((cannon) => {
                        this.cannonFire(cannon.x, cannon.y);
                    });
                    this.nextShotAt = game.time.now + 3000;
                }
                this.bullets.forEachAlive((bullet) => {
                    if(bullet.body.velocity.y > 0) bullet.angle = Math.acos(bullet.body.velocity.x/Math.sqrt(bullet.body.velocity.x*bullet.body.velocity.x+bullet.body.velocity.y*bullet.body.velocity.y))*180/Math.PI;
                    else bullet.angle = 360-Math.acos(bullet.body.velocity.x/Math.sqrt(bullet.body.velocity.x*bullet.body.velocity.x+bullet.body.velocity.y*bullet.body.velocity.y))*180/Math.PI;
                    // console.log(bullet.angle);
                });

                // Bows.
                if(game.time.now > this.nextShotAt2){
                    console.log(this.bows);
                    this.bows.forEachAlive((bow) => {
                        switch(bow.dir){
                            case 0:
                                this.bowFire(bow.x + 40, bow.y, bow.dir);
                            break;
                            case 1:
                                this.bowFire(bow.x + 40, bow.y + 40, bow.dir);
                            break;
                            case 2:
                                this.bowFire(bow.x + 40, bow.y + 80, bow.dir);
                            break;
                            case 3:
                                this.bowFire(bow.x + 40, bow.y + 40, bow.dir);
                            break;
                        }
                    });
                    this.nextShotAt2 = game.time.now + 1000;
                }

                this.controlPlayer();

                // Press B to give up.
                if(game.input.keyboard.isDown(Phaser.Keyboard.B)){
                    this.killPlayer();
                }

                // Check if the player reaches goal.
                if(this.player.x > 2600 && this.player.x < 2800 && this.player.y > 700 && this.player.y < 850){
                    this.arriveGoal();
                }
                
                // If the player is dead or reaches goal, start a new round.
                if(!this.player.alive && !this.player.isArrivedGoal[this.nowPlayer]){
                    this.changeNextPlayer();
                }
            break;
            case 4:
                for(var i=1;i<=game.global.playerNum;i++){
                    if(this.player.isArrivedGoal[i]){
                        this.raisingScore[i].updateCrop();
                    }
                }
            break;
            case 5:
                if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
                    game.state.start('menu');
                }
            break;
        }
    },
    changeNextPlayer: function(){
        if(this.nowPlayer == game.global.playerNum){
            this.nowPlayer = 1;
            this.changeState(4);
        }
        else{
            this.nowPlayer++;
            this.resetPlayer();
        }
        this.player.loadTexture(game.global.character[this.nowPlayer],0,false);
    },
    createObstacle: function(x, y, name){
        if(name == 'ironStructure'){
            this.obstacle = this.obstacles.create(x, y, 'ironStructure1');
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
            this.obstacle = this.obstacles.create(x + 40, y, 'ironStructure2');
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        } else if(name == 'cannon') {
            this.obstacle = this.cannons.create(x, y, name);
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        } else if(name == 'bow_top') {
            this.obstacle = this.bows.create(x, y, name);
            this.obstacle.dir = 0;
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        } else if(name == 'bow_bot') {
            this.obstacle = this.bows.create(x, y, name);
            this.obstacle.dir = 2;
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        } else if(name == 'bow_left') {
            this.obstacle = this.bows.create(x, y, name);
            this.obstacle.dir = 1;
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        } else if(name == 'bow_right') {
            this.obstacle = this.bows.create(x, y, name);
            this.obstacle.dir = 3;
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        } else {
            this.obstacle = this.obstacles.create(x, y, name);
            this.obstacle.scale.setTo(0.5, 0.5);
            this.obstacle.body.immovable = true;
        }
    },
    killPlayer: function(){
        if(this.player.isDead) return;

        this.player.isDead = true;
        this.player.animations.play('dead');

        setTimeout(()=>{
            this.player.kill();
        }, 2000);
    },
    controlPlayer: function(){
        // Set player's acceleration to 0.
        this.player.body.acceleration.x = 0;

        if(!this.player.isDead && !this.player.isArrivedGoal[this.nowPlayer]){
            // Player move.
            if(this.cursor.left.isDown && !this.cursor.right.isDown){
                this.player.body.acceleration.x -= 2000;
                this.player.facingLeft = true;
                if(this.player.body.blocked.down || this.player.body.touching.down){
                    this.player.animations.play('leftwalk');
                } else {
                    if(this.player.body.velocity.y < 0) this.player.frame = 7;
                    else this.player.frame = 9;
                }
            }
            else if(this.cursor.right.isDown && !this.cursor.left.isDown){
                this.player.body.acceleration.x += 2000;
                this.player.facingLeft = false;
                if(this.player.body.blocked.down || this.player.body.touching.down){
                    this.player.animations.play('rightwalk');
                } else {
                    if(this.player.body.velocity.y < 0) this.player.frame = 6;
                    else this.player.frame = 8;
                }
            }
            else {
                this.player.body.velocity.x /= 1.4;
                if(this.player.body.blocked.down || this.player.body.touching.down){
                    if(this.player.facingLeft) this.player.frame = 1;
                    else this.player.frame = 0;
                } else {
                    if(this.player.facingLeft){
                        if(this.player.body.velocity.y < 0) this.player.frame = 7;
                        else this.player.frame = 9;
                    }
                    else {
                        if(this.player.body.velocity.y < 0) this.player.frame = 6;
                        else this.player.frame = 8;
                    }
                }
            }

            // Limit the max speed of the player.
            if(this.player.body.velocity.x > 400) this.player.body.velocity.x = 400;
            if(this.player.body.velocity.x < -400) this.player.body.velocity.x = -400;

            // Check if the player is touching the wall, if yes, give him a friction.
            if(this.player.body.blocked.left || this.player.body.blocked.right || this.player.body.touching.left || this.player.body.touching.right){
                if(this.player.body.velocity.y > 200) this.player.body.velocity.y = 200;
            }

            // Player jump.
            if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
                if(this.player.body.blocked.down || this.player.body.touching.down){
                    this.player.body.velocity.y = -400;
                } else {
                    if(this.player.body.blocked.left || this.player.body.touching.left){
                        this.player.body.velocity.x = 800;
                        this.player.body.velocity.y = -400;
                    }
                    else if(this.player.body.blocked.right || this.player.body.touching.right){
                        this.player.body.velocity.x = -800;
                        this.player.body.velocity.y = -400;
                    }
                }
            }

            // Animations when the player is against the wall.
            if(!this.player.body.blocked.down && !this.player.body.touching.down){
                if(this.player.body.blocked.left || this.player.body.touching.left){
                    this.player.frame = 12;
                }
                else if(this.player.body.blocked.right || this.player.body.touching.right){
                    this.player.frame = 13;
                }
            }
        } else {
            // Player is dead.
            this.player.body.velocity.x = 0;
        }
    },
    arriveGoal: function(){
        if(this.player.isArrivedGoal[this.nowPlayer]) return;
        
        this.player.isArrivedGoal[this.nowPlayer] = true;
        this.player.kill();

        var dance = game.add.sprite(this.player.x, this.player.y, game.global.character[this.nowPlayer]+'Dance');
        dance.scale.setTo(0.55, 0.55);
        dance.anchor.setTo(0.5);
        dance.animations.add('dance');
        dance.play('dance', 10, false, true);
        dance.animations.currentAnim.onComplete.add(function () {	
            if(this.player.score[this.nowPlayer] < 5){
                this.player.score[this.nowPlayer]++;
            };

            this.changeNextPlayer();
        },this);
    },
    resetPlayer: function(){
        this.player.facingLeft = false;
        this.player.isDead = false;
        this.player.reset(600, 700);
    },
    scrollMap: function(){
        if (game.input.y < 100 || this.cursor.up.isDown) game.camera.y -= 10;
        else if (game.input.y > game.height - 100 || this.cursor.down.isDown) game.camera.y += 10;
    
        if (game.input.x < 100 || this.cursor.left.isDown) game.camera.x -= 10;
        else if (game.input.x > game.width - 100 || this.cursor.right.isDown) game.camera.x += 10;
    },
    changeState: function(state){
        this.gameState = state;
        switch(state){
            case 1:
                if(this.player.alive) this.player.kill();
                game.camera.follow(null);
                this.createRepository();
                this.mouseCursor.alpha = 1;
            break;
            case 2:
                if(this.player.alive) this.player.kill();
                game.camera.follow(null);
            break;
            case 3:
                for(var i=1;i<=game.global.playerNum;i++){
                    this.player.isArrivedGoal[i] = false;
                }
                this.nowPlayer = 1;
                this.resetPlayer();
                game.camera.follow(this.player);
                this.mouseCursor.alpha = 0;
            break;
            case 4:
                this.createLeaderboard();
                for(var i=1;i<=game.global.playerNum;i++){
                    if(this.player.isArrivedGoal[i]){
                        this.tween[i].start();
                    }
                }
                setTimeout(()=>{
                    this.scoreFlag = false;
                    this.leaderboard.kill();
                    for(var i=1;i<=game.global.playerNum;i++){
                        this.leaderboard.players[i].kill();
                    }
                    this.scoreBar.forEachAlive(score=>{
                        score.kill();
                    })
                    for(var i=1;i<=game.global.playerNum;i++){
                        if(this.player.isArrivedGoal[i]){
                            this.tween[i].stop();
                        }
                    }
                    for(var i=1;i<=game.global.playerNum;i++){
                        if(this.player.score[i]==5){
                            this.scoreFlag = true;
                            this.changeState(5);
                            break;
                        }
                    }
                    if(!this.scoreFlag){
                        this.changeState(1);
                    }
                },2000);
            break;
            case 5:
                game.camera.follow(null);
                game.camera.x = 2120;
                game.camera.y = 569;
                var dance = [];
                for(var i=1, j=0;i<=game.global.playerNum;i++){
                    if(this.player.score[i] == 5){
                        dance[i] = game.add.sprite(game.camera.x + 480 + j * 100,game.camera.y + 271, game.global.character[i]+'Dance');
                        dance[i].scale.setTo(0.55, 0.55);
                        dance[i].anchor.setTo(0.5);
                        dance[i].animations.add('dance');
                        dance[i].play('dance', 10, true, true);
                        j++;
                    }
                }
            break;
        }
    },
    createRepository: function(){
        let offsetX = 0;
        let offsetY = 0;
        this.items.forEach(function(item){
            switch(item.key){
                case 'board':
                    offsetX = 160;
                    offsetY = 80;
                break;
                case 'boardCol':
                    offsetX = 80;
                    offsetY = 160;
                break;
                case 'box':
                    offsetX = 120;
                    offsetY = 120;
                break;
                case 'metal':
                    offsetX = 180;
                    offsetY = 140;
                break;
                case 'straw':
                    offsetX = 280;
                    offsetY = 160;
                break;
                case 'thorn':
                    offsetX = 120;
                    offsetY = 160;
                break;
                case 'ironStructure':
                    offsetX = 140;
                    offsetY = 300;
                break;
                case 'cannon':
                    offsetX = 120;
                    offsetY = 200;
                break;
                case 'bow_top':
                    offsetX = 140;
                    offsetY = 300;
                break;
                case 'bow_bot':
                    offsetX = 200;
                    offsetY = 300;
                break;
                case 'bow_left':
                    offsetX = 260;
                    offsetY = 300;
                break;
                case 'bow_right':
                    offsetX = 320;
                    offsetY = 300;
                break;
                default:
                    offsetX = 500;
                    offsetY = 500;
                break;
            }
            item.reset(game.camera.x + offsetX - 800, game.camera.y + offsetY);
            game.add.tween(item).to({x: game.camera.x + offsetX}, 1000).start();
        });
        this.repository.reset(game.camera.x - 800, game.camera.y);
        this.repository.alpha = 1;
        game.add.tween(this.repository).to({x: game.camera.x}, 1000).start();
    },
    collideObstacle: function(player, item){
        if(item.key === 'thorn'){
            this.killPlayer();
        }
    },
    createLeaderboard: function(){
        /*
        x = game.camera.x+218 ->first
        x = game.camera.x+326 ->second
        */
        this.leaderboard.reset(game.camera.x-55,game.camera.y-35);
        for(var i=1;i<=game.global.playerNum;i++){
            this.leaderboard.players[i].reset(game.camera.x+30,game.camera.y+90*i);
        }

        for(var j=1;j<=game.global.playerNum;j++){
            if(this.player.isArrivedGoal[j]){
                for(var i=0;i<this.player.score[j]-1;i++){
                    var score = this.scoreBar.getFirstExists(false);
                    score.reset(game.camera.x+218+108*i,game.camera.y+150+(j-1)*87);
                }

                this.crop[j] = new Phaser.Rectangle(0,0,0,this.scoreTest.height);
                this.raisingScore[j] = this.scoreBar.getFirstExists(false);
                this.raisingScore[j].reset(game.camera.x+218+108*(this.player.score[j]-1),game.camera.y+150+(j-1)*87);
                this.tween[j] = game.add.tween(this.crop[j]).to({width:this.scoreTest.width},600);
                this.raisingScore[j].crop(this.crop[j]);
            }
            else{
                for(var i=0;i<this.player.score[j];i++){
                    var score = this.scoreBar.getFirstExists(false);
                    score.reset(game.camera.x+218+108*i,game.camera.y+150+(j-1)*87);
                }
            }
        }

    },
    cannonFire: function(x, y){
        this.bullet = this.bullets.getFirstExists(false);

        if (!this.bullet) return;
        
        this.bullet.reset(x, y);
        this.bullet.body.velocity.x = -500;
        this.bullet.body.velocity.y = -500;
        this.bullet.body.acceleration.y = 1000;
    },
    bulletHitPlayer: function(player, bullet){
        bullet.kill();
        this.killPlayer();
        this.explode(bullet.x, bullet.y);
    },
    bulletHitGround: function(bullet, ground){
        bullet.kill();
        this.explode(bullet.x, bullet.y);
    },
    explode: function(x, y){
        var explosion = game.add.sprite(x, y, 'explode');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
    },
    bowFire: function(x, y, dir){
        switch(dir){
            case 0:
                this.arrow = this.arrows.getFirstExists(false);
            break;
            case 1:
                this.arrow = this.arrows_left.getFirstExists(false);
            break;
            case 2:
                this.arrow = this.arrows_bot.getFirstExists(false);
            break;
            case 3:
                this.arrow = this.arrows_right.getFirstExists(false);
            break;
        }
        

        if (!this.arrow) return;
        
        this.arrow.reset(x, y);
        switch(dir){
            case 0:
                this.arrow.body.velocity.y = -500;
            break;
            case 1:
                this.arrow.body.velocity.x = -500;
            break;
            case 2:
                this.arrow.body.velocity.y = 500;
            break;
            case 3:
                this.arrow.body.velocity.x = 500;
            break;
        }
    }
}

//960 540--->3280 1320
var game = new Phaser.Game(960, 540, Phaser.AUTO, 'canvas');
game.global = {
    playerNum: 4,
    character: []
}
game.state.add('play', playState);
game.state.add('menu', menuState);
game.state.add('maps', mapsState);
game.state.add('multi',multiplayState);
game.state.start('menu');